require 'test/unit'
require 'parser/parser.rb'

class PaserUnitTest < Test::Unit::TestCase

  attr_accessor :parser

  def setup
    @parser = Parser.new("tests/fixture_games.log")
    @parser.parse_games
  end

  def test_should_raise_exception_for_non_args_or_bad_args_constructor
    assert_raise(ArgumentError) { Parser.new }
    assert_raise(ArgumentError) { Parser.new("anything.log")}
  end

  def test_should_return_number_of_games
    assert_equal 2, @parser.games.count
  end

  def test_game_one_should_have_2_players
    assert_equal 2, @parser.games.first.players.count
  end

  def test_game_should_have_11_kills
    assert_equal 11, @parser.games.first.total_kills
  end

  def test_Isgalamido_should_have_none_kills_cause_penalty_of_death_by_world
    assert_equal 0, @parser.games.first.kills["Isgalamido"]
  end

  def test_should_return_number_of_kills_by_means
    assert_not_nil(@parser.games.first.kills_by_means)
  end

  def test_shuould_return_7_kills_by_MOD_TRIGGER_HURT
    assert_equal(7, @parser.games.first.kills_by_means['MOD_TRIGGER_HURT'])
  end
end
