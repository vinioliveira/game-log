class Game

  attr_accessor :total_kills, :players, :kills, :kills_by_means

  def initialize
    @players = []
    @kills = {}
    @kills_by_means = {}
    @total_kills = 0
  end

  def count_kills players

    killer = players[:killer]
    killed = players[:killed]

    #if the killer is an unknown player the killed player should be penalized
    if @players.include? killer
      if self.kills.include? killer
        self.kills[killer] += 1
      else
        self.kills[killer] = 1
      end
    elsif self.kills.include? killed
      self.kills[killed] -= 1 if self.kills[killed] > 0
    end
  end

  def to_json(*a)
    { total_kills: total_kills , players: players, kills: kills, kills_by_means: kills_by_means }.to_json(*a)
  end
end
