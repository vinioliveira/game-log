class Ranking

  attr_accessor :games

  def initialize games
    @games = games
  end

  def ranking_of_players
    ranking = {}
    @games.each do |game|
      game.kills.each do |player, total_kills|
        if ranking[player]
          ranking[player] += total_kills
        else
          ranking[player] = total_kills
        end
      end
    end
    ranking.sort_by{|k,v| v}.reverse
  end

end
