require 'report/report'
require 'parser/parser'
require 'models/ranking'

parser =  Parser.new('games.log')
games = parser.parse_games
report = Report.new
rank = Ranking.new(games)
report.print_rank rank.ranking_of_players
report.print_game games




