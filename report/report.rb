require 'report/console_report'

class Report

  attr_accessor :printer

  def initialize
    @printer = ConsoleReport.new
  end

  def print_game games
    @printer.print_game games
  end

  def print_rank rank
    @printer.print_rank rank
  end

end
