require 'json'
class ConsoleReport

  def print_game games
    games.each_with_index do |game, index|
      print "game_#{index+1}: #{JSON.pretty_generate(game)}\n"
    end
  end

  def print_rank rank
    format = "%-20s%10s\n"
    printf(format, "Players", "Total Kills")
    printf(format, "-------", "-----------")

    rank.each do |player, total_kills|
      printf(format, player, total_kills)
    end
  end

end
