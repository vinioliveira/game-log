require 'models/game'
require 'parser/file_log_game_parser'

class Parser

  attr_reader :games ,:game_log_path
  attr_accessor :interpreter

  def initialize path
    raise ArgumentError unless File.exists?( path )
    @game_log_path = path
    @interpreter = FileLogGameParser.new
  end

  def parse_games
    game_log = File.open(@game_log_path)
    @interpreter.parse(game_log)
    game_log.close
    @games = @interpreter.games
  end

end



