class FileLogGameParser

  REGEX_KILLS_REGISTER = /([\w\d]+)? killed ([\w\d]+)/
  REGEX_CAUSE_OF_DEATH = /by ([\w\d]+)?/
  GAME_START_KEYWORD =  "InitGame"
  KILL_KEYWORD =  "Kill"

  attr_reader :games

  def initialize
    @games = []
  end

  def parse log

    current_game = nil

    log.each_line do |line|

      if line.include? GAME_START_KEYWORD
        current_game = create_new_game
      elsif line.include? KILL_KEYWORD
        increment_kills_of_game(current_game)
        players = fetch_players(line)
        register_players_to_game(current_game,players)
        cause_of_death = fetch_cause_of_death(line)
        register_deaths_by_means_to_game(current_game, cause_of_death)
        current_game.count_kills(players)
      end
    end
  end


  def create_new_game
    new_game = Game.new
    @games << new_game
    new_game
  end

  def fetch_players line
    line =~ REGEX_KILLS_REGISTER
    {killer:$1, killed:$2}
  end

  def fetch_cause_of_death line
    line =~ REGEX_CAUSE_OF_DEATH
    $1
  end

  def register_deaths_by_means_to_game game, cause_of_death
    if game.kills_by_means.has_key? cause_of_death
      game.kills_by_means[cause_of_death] += 1
    else
      game.kills_by_means[cause_of_death] = 1
    end
  end

  def register_players_to_game game, players
    players.each_value do |player|
      if player and !game.players.include? player
        game.players << player
      end
    end
  end

  def increment_kills_of_game game
    game.total_kills += 1
  end

end
